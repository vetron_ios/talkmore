//
//  XMPPHandler.swift
//  TalkMore
//
//  Created by Vetron Services on 12/07/18.
//  Copyright © 2018 Vetron Services. All rights reserved.
//

import UIKit
import Foundation
import XMPPFramework
import UserNotifications

//MARK:- Enum Declare
//MARK:-

enum XMPPMyStatus : Int {
    case available
    case unavailable
    case away
    case invisible
    case offline
    case subscribe
    case subscribed
    case unsubscribe
    case unsubscribed
    case custome
}

enum XMPPChatState : Int {
    case active
    case composing
    case paused
    case inactive
    case gone
}

enum XMPPMessageSender: Int {
    case myself
    case someone
}

enum XMPPMessageState : Int {
    case sending
    case sent
    case received
    case read
    case failed
    case waiting
    case delivered
}

enum XMPPJidType : Int {
    case single
    case group
    case channel
}

enum XMPPMessageType : Int {
    case text
    case image
    case audio
    case video
    case contact
    case location
    case doc
    case link
    case txtImage
    case txtAudio
    case txtVideo
    case txtLocation
    case txtDoc
    case txtLink
    case voice
}

enum XMPPConnectError: Error {
    case wrongUserJID
}

//MARK:- Protocol of Chatbuddy
//MARK:-

protocol ChatXMPPDelegate {
    func buddyWentOnline(name: String, Identifier: String, MessageType: MessageType, Sender: MessageSender, Status: MessageStatus, Date: Date)
    func buddyWentOffline(name: String, Identifier: String, MessageType: MessageType, Sender: MessageSender, Status: MessageStatus, Date: Date)
    func GetMesageonline()
    func didDisconnect()
}

//MARK:- User Custome Delegate methods
//MARK:-

@objc protocol XMPPUserDelegate {

    @objc optional func xmppStreamDidConnect()
    @objc optional func xmppStreamDidDisconnect()
    @objc optional func xmppStreamDidAuthenticate()
    @objc optional func DidReceivePersonalInformation()
    @objc optional func DidUpdateRecentlyChattingList()

    //    Reconnect Time Delegate
    @objc optional func clearReconnectionTime()
    @objc optional func addReconnectionTime()

    //    Last Activity for friends
    @objc optional func getLastActivityForFriendWithJIDNoAsyn(jid: String)

    @objc optional func RegisterAPNS(token: String)

    //    XMPP Business
    @objc optional func DidReceivedFriendSubscribed(sender: XMPPStream, with message: XMPPMessage)
    @objc optional func DidReceivedFriendAcceptedMyApply(sender: XMPPStream, with message: XMPPMessage)

    //    XMPP Message
    @objc optional func DealWithReceivedFriendMsg(message: XMPPMessage)
    @objc optional func DealWithGroupMsg(message: XMPPMessage, withUpdateTheUI updateUI: Bool, withStamp stamp: String)
    @objc optional func DealWithGroupInviteMsg(message: XMPPMessage)
    @objc optional func GetJidWithNoSlash(jid: String) -> String
}

//MARK:- XMPPHandler Class

class XMPPHandler: NSObject {

    let XMPP_Handler = UIApplication.shared

    static let sharedInstance: XMPPHandler = {
        let instance = XMPPHandler()
        // setup code
        return instance
    }()

    //    MARK:- Variable Declare
    //    MARK:-

    var messagetype: XMPPMessageType = .text
    var mystatus: XMPPMyStatus = .available
    var chatstate: XMPPChatState = .active
    var messagesender: XMPPMessageSender = .someone
    var messagestate: XMPPMessageState = .sending
    var jidtype: XMPPJidType = .single

    var xmppStream: XMPPStream!
    var xmppStreamManage: XMPPStreamManagement!
    let xmppRosterStorage = XMPPRosterCoreDataStorage()
    var xmppRoster: XMPPRoster!
    var xmppMessage: XMPPMessage!

    //    Custome Delegate
    var ChatXMPPDelegate: ChatXMPPDelegate!
    var XMPPUserDelegate: XMPPUserDelegate!

    //XMPPSteam
    var xmppReconnect: XMPPReconnect!

    //XMPPRoster
    var xmppRosterCoreDataStorage: XMPPRosterCoreDataStorage?

    //XMPPvCard
    var xmppvCardCoreDataStorage: XMPPvCardCoreDataStorage?
    var xmppvCardTemp: XMPPvCardTempModule?
    var xmppvCardAvatar: XMPPvCardAvatarModule?

    //Capabilities
    var xmppCapabilities: XMPPCapabilities?
    var xmppCapabilitiesCoreDataStorage: XMPPCapabilitiesCoreDataStorage?
    var xmppAutoPing: XMPPAutoPing?

    //File Upload
    var xmppHTTPFileUpload: XMPPHTTPFileUpload?

    //OfflineMessage
    var xmppOfflineMessage: XMPPOfflineMessage?

    var hostName: String!
    var userJID: XMPPJID!
    var hostPort: UInt16!
    var password: String!

    //    MARK:- User Chat History and Contact details
    var ChatMessage: Message!
    var ChatContact: Contact!
    var Chat: Chat!

    // MARK: - Initialization Method
    override init() {
        super.init()
        self.SetupXmpp()
    }

    //    MARK:- SetupXMPP Connection
    //    MARK:-

    func SetupXmpp() {
        if Utility.isConnectedToInternet() {
            self.hostName = XMPP_HOST_NAME
            self.userJID = XMPPJID.init(string: JID1)
            self.hostPort = XMPP_PORT
            self.password = JID_Password

            // Stream Configuration
            self.xmppStream = XMPPStream()
            self.xmppStream.hostName = hostName
            self.xmppStream.hostPort = hostPort
            self.xmppStream.myJID = XMPPJID(string: JID1)
            self.xmppStream.enableBackgroundingOnSocket = true

            self.xmppStream.supportsPush()
            self.xmppStream.supportsRebind()
            self.xmppStream.keepAliveInterval = 600

            let XmppMemory = XMPPStreamManagementMemoryStorage()
            self.xmppStreamManage = XMPPStreamManagement.init(storage: XmppMemory)
            self.xmppStreamManage.autoResume = true
            self.xmppStreamManage.enable(withResumption: true, maxTimeout: 100000)
            xmppStreamManage.activate(self.xmppStream)
            self.xmppStreamManage.addDelegate(self, delegateQueue: DispatchQueue.main)

            xmppStream.performSelector(onMainThread: Selector(("setIsSecure:")), with: NSNumber.init(value: 1), waitUntilDone: true)
            if !self.xmppStream.isConnected() {
                do {
                    try xmppStream.connect(withTimeout: XMPPStreamTimeoutNone)
                    xmppStream.enableBackgroundingOnSocket = true

                    //initialize XMPPRosterCoreDataStorage
                    xmppRoster = XMPPRoster(rosterStorage: xmppRosterStorage)
                    self.xmppRoster.activate(self.xmppStream)
                    xmppRosterCoreDataStorage = XMPPRosterCoreDataStorage.sharedInstance()
//                    xmppRoster = XMPPRoster(rosterStorage: xmppRosterCoreDataStorage, dispatchQueue: DispatchQueue.main)
                    xmppRoster.autoClearAllUsersAndResources = false
                    xmppRoster.autoFetchRoster = false

                    self.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
                    self.xmppRoster.addDelegate(self, delegateQueue: DispatchQueue.main)

                    xmppReconnect = XMPPReconnect()
                    xmppReconnect.autoReconnect = true
                    xmppReconnect.reconnectDelay = 0.0

                    xmppReconnect.reconnectTimerInterval = 3.0

                    xmppReconnect.activate(xmppStream)
                    xmppReconnect.addDelegate(self, delegateQueue: DispatchQueue.main)

                    //OfflineMessage
                    xmppOfflineMessage = XMPPOfflineMessage()
                    xmppOfflineMessage?.activate(xmppStream)
                    xmppOfflineMessage?.addDelegate(self, delegateQueue: DispatchQueue.main)

                    print("Connection success")
                    self.goOnline()
                } catch {
                    print("Something went wrong to connect XMPP!")
                }
            }
            else{
                self.goOnline()
            }
        }
        else{
            App.window?.rootViewController?.Display_Alert(msg: "No! internet connection available", title_str: "Internet Access")
        }
    }

    //    MARK:- Check XMPP Connection
    func CheckXMPPConnection() {
        if Utility.isConnectedToInternet() {
            if !xmppStream.isConnected() {
                if xmppStream.isDisconnected() {
                    self.connect()
                }
                else{
                    self.goOnline()
                }
            }
            else{
                self.connect()
            }
        }
        else{
            App.window?.rootViewController?.Display_Alert(msg: "No! internet connection available", title_str: "Internet Access")
        }
    }

    func clearXMPPStream() {
        //remove delegate
        xmppStream.removeDelegate(self)
        xmppRoster.removeDelegate(self)
        xmppReconnect.deactivate()
        xmppRoster.deactivate()
        xmppStream.disconnect()
        //clear objects
        xmppStream = nil
        xmppReconnect = nil
        xmppRoster = nil
        //    xmppCapabilities = nil;
        //    xmppCapabilitiesCoreDataStorage = nil;
        //        netWork = nil
    }

    //    MARK:- Connect XMPP Server
    func connect() {
        if !xmppStream.isConnected() {

            if !xmppStream.isDisconnected() {
                return
            }

            if JID1.count == 0 && JID_Password.count == 0 {
                return
            }

            self.xmppStream = XMPPStream()
            self.xmppStream.hostName = hostName
            self.xmppStream.hostPort = hostPort
            self.xmppStream.myJID = XMPPJID(string: JID1)

            do {
                xmppStream.performSelector(onMainThread: Selector(("setIsSecure:")), with: NSNumber.init(value: 1), waitUntilDone: true)
                try xmppStream.connect(withTimeout: XMPPStreamTimeoutNone)
                self.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
                self.xmppRoster.addDelegate(self, delegateQueue: DispatchQueue.main)
                print("Connection success")
                self.goOnline()
            } catch {
                print("Something went wrong to connect XMPP!")
            }

        }
    }

    func disconnect() {
        goOffline()
        xmppStream.disconnect()
        self.clearXMPPStream()
        print("Disconnect Success")
    }

    func goOnline() {
        let presence = XMPPPresence()
        let domain = xmppStream.myJID?.domain

        if !xmppStream.isConnected() {
            //        if domain == "gmail.com" || domain == "gtalk.com" || domain == "talk.google.com" || domain == "jabber.org" {
            if domain == "dev.talkmoreapp.net" {
                let priority = DDXMLElement.element(withName: "priority", stringValue: "24") as! DDXMLElement
                presence?.addChild(priority)
                print("Goonline Success")
            }
            xmppStream.send(presence)
        }
        else{
            print("You are allready online")
        }
    }

    func goOffline() {
        let presence = XMPPPresence(type: "unavailable")
        xmppStream.send(presence)
        DispatchQueue.main.async(execute: {() -> Void in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Dismiss_Progress), object: self, userInfo: nil)
        })
    }

}

//MARK:- GCDAsyncSocketDelegate
//MARK:-

extension XMPPHandler: GCDAsyncSocketDelegate{
    func xmppStream(_ sender: XMPPStream, socketDidConnect socket: GCDAsyncSocket) {
        print("GCDAsyncSocket connected successful")
    }
}

//MARK:- XMPPStreamDelegate
//MARK:-

extension XMPPHandler: XMPPStreamDelegate{

    func xmppStreamDidConnect(_ stream: XMPPStream) {
        print("Stream: Connected")
        do {
            try xmppStream.authenticate(withPassword: JID_Password)
        }
        catch{
            print("Stream: Fail to Authenticate")
        }
    }

    func xmppStreamDidDisconnect(_ sender: XMPPStream!, withError error: Error!) {
        print("Stream: xmppStreamDidDisconnect")
        if (error != nil) {
            let description = error.localizedDescription
            if description == "Socket closed by remote peer" {
                self.connect()
            }
        }
    }

    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
        print("Stream: Authenticated")
        sender.send(XMPPPresence())
    }

    func xmppStream(_ sender: XMPPStream, didSend message: XMPPMessage) {
        print("Did send message \(message)")
    }

    func xmppStream(_ sender: XMPPStream!, didFailToSend message: XMPPMessage!, error: Error!) {

    }

    func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage) {
        print("Stream: didReceive XMPPMessage")
        print(message)
        //        <message xmlns="jabber:client" to="test100030@dev.talkmoreapp.net" from="test100002@dev.talkmoreapp.net/Vetrons-iMac" type="chat" id="purpleeed852c3"><active xmlns="http://jabber.org/protocol/chatstates"></active></message>
//        <message xmlns="jabber:client" to="test100030@dev.talkmoreapp.net" from="test100002@dev.talkmoreapp.net/Vetrons-iMac" type="chat" id="purple22e79010"><active xmlns="http://jabber.org/protocol/chatstates"></active><body>fsdfsadfsadfsda</body></message>
        if message.hasActiveChatState(){

        }

        if message.isChatMessage(){

        }
        else if message.isChatMessageWithBody(){

        }
    }

    func xmppStream(_ sender: XMPPStream, didReceive presence: XMPPPresence) {
        print("Stream: Did receive XMPPPresence \(presence)")
//        <presence xmlns="jabber:client" to="test100030@dev.talkmoreapp.net/8546688884842273984103536" from="test100003@dev.talkmoreapp.net/Vetrons-iMac"><c xmlns="http://jabber.org/protocol/caps" node="http://pidgin.im/" hash="sha-1" ver="DdnydQG7RGhP9E3k9Sf+b+bF0zo="></c><x xmlns="vcard-temp:x:update"><photo>16fff9b3f306d06fa43c0f47f70fd8053b224c24</photo></x></presence>
        let presenceType = presence.type()
        let myUsername = sender.myJID?.user
        print(myUsername as Any)
        let presenceFromUser = presence.from().user
        let bar: String = presence.from().bare()
        let jid: String = self.XMPPUserDelegate.GetJidWithNoSlash!(jid: presence.from().full())
        if presence.isErrorPresence() {
            print("XMPPStreamDelegate : Receive error");
        }else if bar == xmppStream.myJID.full() {
            print("XMPPStreamDelegate : Receive Self Presence.");
        }
        else if presenceType == "subscribe" {
            print("XMPPStreamDelegate : Receive Subscribe Presence.")
            let xmppjid: XMPPJID = XMPPJID.init(string: jid)
            xmppRoster.acceptPresenceSubscriptionRequest(from: xmppjid, andAddToRoster: true)
        }
        else if presenceType == "subscribed" {
            print("XMPPStreamDelegate : Receive Subscribed Presence.")
        }
        else if presenceType == "unsubscribe" {
            print("XMPPStreamDelegate : Receive Unsubscribe Presence.")
        }
        else if presenceType == "unsubscribed" {
            print("XMPPStreamDelegate : Receive Unsubscribed Presence.")
        }
        else{
            if presenceType == "available" {
                let contact = Contact()
                contact.name = String(format: "%@", presenceFromUser!)
                contact.identifier = String(format: "%@@%@", presenceFromUser!, hostName)
                contact.imagename = "soapbubble.jpg"
                contact.imageurl = ""

                ChatXMPPDelegate.buddyWentOnline(name: String(format: "%@", presenceFromUser!), Identifier: String(format: "%@@%@", presenceFromUser!, hostName), MessageType: .text, Sender: .someone, Status: .received, Date: Date())
            }
            else if presenceType == "unavailable" {
                ChatXMPPDelegate.buddyWentOffline(name: String(format: "%@", presenceFromUser!), Identifier: String(format: "%@@%@", presenceFromUser!, hostName), MessageType: .text, Sender: .myself, Status: .sending, Date: Date())
            }
        }
        sender.send(XMPPPresence())
    }

    func xmppStream(_ sender: XMPPStream, didNotAuthenticate error: DDXMLElement) {
        print("Stream: Fail! didNotAuthenticate")
        do {
            try xmppStream.authenticate(withPassword: JID_Password)
        }
        catch{
            print("Stream: Fail to Authenticate")
        }
    }

    func xmppStream(_ sender: XMPPStream, didReceive iq: XMPPIQ) -> Bool {
        print("Stream: Did receive XMPPIQ")
        print(iq)
//        <iq xmlns="jabber:client" to="test100030@dev.talkmoreapp.net/11298281827444387734102370" from="test100030@dev.talkmoreapp.net" type="result" id="FB8F21AD-13C5-4DCF-AF10-ECB4A3073262"></iq>
        if iq.isResultIQ() {

        }
        return false
    }

    func xmppStreamDidAuthenticate(sender: XMPPStream!) {
        print("Stream: DidAuthenticate")
        do {
            try xmppStream.authenticate(withPassword: JID_Password)
        }
        catch{
            print("Stream: Fail to Authenticate")
        }
        self.goOnline()
    }

}

//MARK:- XMPPReconnectDelegate
//MARK:-

extension XMPPHandler: XMPPReconnectDelegate{

    func xmppReconnect(_ sender: XMPPReconnect!, didDetectAccidentalDisconnect connectionFlags: SCNetworkConnectionFlags) {
        print("Reconnect detectaccidentalDisconnect \(connectionFlags)")
    }

    func xmppReconnect(_ sender: XMPPReconnect!, shouldAttemptAutoReconnect connectionFlags: SCNetworkConnectionFlags) -> Bool {
        print("Reconnect shouldattemptautoreconnect \(connectionFlags)")
        return false
    }

}

//MARK:- XMPPHTPPFileUploadDelegate
//MARK:-

extension XMPPHandler: XMPPHTPPFileUploadDelegate{

    func xmppHTTPFileUpload(_ sender: XMPPHTTPFileUpload!, didAssign slot: XMPPSlot!) {
        print("Reconnect xmpphttpfileupload didassign \(slot)")
    }

    func xmppHTTPFileUpload(_ sender: XMPPHTTPFileUpload!, didFailToAssignSlotWithError iqError: XMPPIQ!) {
        print("Reconnect xmpphttofileupload didfailtoassignslotwitherror \(iqError)")
    }

    func getofflineDatekey() -> String {
        return String(format: "%@_Offline", xmppStream.myJID.full())
    }

}

//MARK:- XMPPOfflineMessageDelegate
//MARK:-

extension XMPPHandler: XMPPOfflineMessageDelegate{

    func didReceivedNumber(ofOfflineMessages count: Int, with iq: XMPPIQ!) {
        print("Reconnect xmppofflinemessage count \(iq)")
    }

    func fetchofflineMessage() {

    }

    func didReceivedAll(ofOfflineMessages iqElement: XMPPIQ!) {
        print("Reconnect didreveivedallofofflinemessages \(iqElement)")
    }

    func didRemovedAllofOfflineMessages(_ status: Bool, with iq: XMPPIQ!) {
        print("Reconnect didremovedallofofflinemessages \(iq)")
    }

    func didRetrivedMessageHeader(_ iqElement: XMPPIQ!) {
        print("Reconnect didretrivedmessageheader \(iqElement)")
    }

    func xmppRoster(_ sender: XMPPRoster, didReceiveRosterItem item: DDXMLElement) {
        print("Did receive Roster item")
    }

}
